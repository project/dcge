<?php

namespace Drupal\dcge\Generators;

use Drupal\dcge\Traits\Form\RouteInteractionTrait;
use DrupalCodeGenerator\Command\BaseGenerator;
use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class FormGenerator extends BaseGenerator {

  use RouteInteractionTrait;

  /**
   * {@inheritdoc}
   */
  protected $name = 'form:generate';

  /**
   * {@inheritdoc}
   */
  protected $description = 'Generates Form from a blueprint template.';

  /**
   * {@inheritdoc}
   */
  protected $alias = 'gnform';

  /**
   * {@inheritdoc}
   */
  protected $templatePath = __DIR__ . '/../../templates';

  /**
   * {@inheritdoc}
   */
  protected function interact(InputInterface $input, OutputInterface $output): void {
    // Determine the module to add our form to.
    $questions = Utils::moduleQuestions();

    // Suggest default class name: MyModuleForm
    $defaultClass = function($vars) {
      return Utils::camelize($vars['machine_name']) . 'Form';
    };
    // Add a question to the question list.
    $questions['class'] = new Question('Class', $defaultClass);
    // Ask the questions
    $this->collectVars($input, $output, $questions);

    // Check if user wants to add services.
    $diQuestion = new ConfirmationQuestion(
      'Would you like to inject dependencies?', FALSE
    );
    if ($this->ask($input, $output, $diQuestion)) {
      // Add services.
      $this->collectServices($input, $output);
    }

    // Generate Route
    $this->routeInteraction($input, $output);

    $this->addFile()
      ->path('src/Form/{class}.php')
      ->template('/form/form.twig');
  }

}
