<?php

namespace Drupal\dcge\Traits\Form;

use DrupalCodeGenerator\Utils;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

/**
 * Use this trait to generate Routes.
 */
trait RouteInteractionTrait {

  /**
   * Interacts with the user and builds route variables.
   */
  protected function routeInteraction(InputInterface $input, OutputInterface $output): void {
    $vars = &$this->vars;

    $routeQuestion = new ConfirmationQuestion(
      'Would you like to create a route for this form?'
    );
    $vars['route'] = $this->ask($input, $output, $routeQuestion);

    $rawFormId = preg_replace(
      '/_form/',
      '',
      Utils::camel2machine($vars['class'])
    );
    $vars['form_id'] = $vars['machine_name'] . '_' . $rawFormId;

    if ($vars['route']) {
      $defaultRoutePath = str_replace(
        '_',
        '-',
        $vars['machine_name'] . '/' . $rawFormId
      );
      $routeQuestions['route_name'] = new Question(
        'Route name',
        '{machine_name}.' . $rawFormId
      );
      $routeQuestions['route_path'] = new Question(
        'Route path',
        $defaultRoutePath
      );
      $routeQuestions['route_title'] = new Question(
        'Route title',
        Utils::machine2human($rawFormId)
      );
      $routeQuestions['route_permission'] = new Question(
        'Route permission',
        'administer site configuration'
      );

      $this->collectVars($input, $output, $routeQuestions, $vars);

      $this->addFile()
        ->path('{machine_name}.routing.yml')
        ->template('form/routing.twig')
        ->action('append');
    }
  }

}
