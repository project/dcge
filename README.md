INTRODUCTION:
--------------------
This is an example module, explaining step by step how you can create
your own drush generate command.

Workings with drush 10.x <br>
Do not enable this module in production environment!

BACKGROUND:
---------------------

This module is inspired
by [Louis Nagtegaal](https://www.drupal.org/u/louisnagtegaal)
[article](https://www.limoengroen.nl/en/news/lazy-coding-exploring-drush-generate)
and [Jacob Rockowitz](https://www.drupal.org/u/jrockowitz) [module](https://www.drupal.org/sandbox/jrockowitz/3262123).

As [Louis](https://www.drupal.org/u/louisnagtegaal) mentioned in his article:

For Drupal site builders, drush is the Swiss army knife that can be used for a wide range of purposes,
from simply clearing the cache to updating translations or even installing a complete new site.

In the more recent versions of drush, a command was added that makes drush also interesting for Drupal developers.
This command, **generate**, makes it easy to generate **boilerplate** code for modules, plugins, services and much more.
And it is even possible to extend existing custom (or contrib) modules with a generator.

The generate command uses
the [chi-teck/drupal-code-generator-library](https://github.com/Chi-teck/drupal-code-generator).

The great strength of the generate command is the way it creates code according to the answers to a number of questions
asked when run.
These questions usually include the machine name of the module you want to add the code to,
the machine name of the plugin that is created, which services you want to inject or which routes you want to add, etc...

-----------

After some research and due to lack of documentation on how to create your own generated command,
I decided to create a custom module explaining in details how you can generate your custom code using drush generate
feature. <Br>
I will use [Examples for Developers module](https://www.drupal.org/project/examples) as an example.


GOAL
------------
Developers can learn how to generate custom blueprint templates
by experimenting with those examples, and adapt them for their own use.

How can you help?
------------------
I'm glad you asked:

Most of all, we need you to file an issue every time you find an error or ambiguity or something that could be presented better.
* We need additional examples.
* We need reviews, corrections, and improvements to existing code.
* We need improvements in the documentation.

HOW GENERATE WORK:
------------
Drush generate is very helpful if you want to create a blueprint twig templates,
a custom class extending from BaseGenerator class, then you write your questions with your custom workflow.

Once all questions are answered, drush will generate your code.

HOW TO:
----------

1. How to integrate your code inside an existing module? <br>
   Let say you want to generate a custom service inside a module you already have,
   what you will do is using a list of questions already defined:
   ``` php
   $questions = Utils::moduleQuestions();
   ```
2. How to ask the user to define a class name:

   ``` php
   $questions['class'] = new Question('Class', 'ExampleClass');
   ```

3. How to generate a default class name based on the module machine_name:

   ``` php
   $defaultClass = function ($vars) {
     return Utils::camelize($vars['machine_name']) . 'Form';
   };

   $questions['class'] = new Question('Class', $defaultClass);
   ```
4. How to get the answers: <br>
   ``` php
   $this->collectVars($input, $output, $questions);
   ```
5. How to ask a confirmation question and get the answer <br>
   ``` php
   $route_question = new ConfirmationQuestion(
     'Would you like to create a route for this form?'
   );

   $answer = $this->ask($input, $output, $route_question);
   ```
6. How to ask the user if he wants to inject dependencies, and collect his answer: <br>
   ``` php
   // Inject Dependencies
   $di_question = new ConfirmationQuestion('Would you like to inject dependencies?', FALSE);

   if ($this->ask($input, $output, $di_question)) {
     $this->collectServices($input, $output);
   }
   ```

7. How to generate the template: <br>
   ``` php
   $this->addFile()
   ->path('src/Batch/{class}.php')
   ->template('/Form/form.twig');
   ```
8. How to define a default template folder in the racine of your module:
   ``` php
   protected $templatePath = __DIR__ . '/../../templates';
   ```

Let's start
-----------
In this example, we will generate a custom form with routing, and we will
inject some dependencies.

Before we start, we need to:

1. Create a templates folder in our module root:
   `/my_module/templates`.
2. You need to copy the `/templates/lib` folder from this module inside your module templates folder.
3. Create a php class under `src/Generators/FormGenerator.php`
4. This class must extends `BaseGenerator` class:

    ```php
    use DrupalCodeGenerator\Command\BaseGenerator;

    class FormGenerator extends BaseGenerator { }
    ```
5. We need to implement the `interact` method, we will need it to interact with the user:

    ```php
    protected function interact(InputInterface $input, OutputInterface $output) { }
    ```
6. Inside our class `FormGenrator` we need to define our command name,description, alias and the template path:

    ```php
    protected $name = 'form:generate';
    protected $description = 'Generates Form from a blueprint template.';
    protected $alias = 'gnform';
    protected $templatePath = __DIR__ . '/../../templates';
    ```
7. Declare our class `FormGenerator` as drush generator command, <br>
   you need to `create/update` the `drush.services.yml` file:
    ```yml
    services:
      my_module.commands:
        class: Drupal\my_module\Generators\FormGenerator
        tags:
          - { name: drush.generator }
    ```

**Now let the fun begin:** <br>
Inside the `interact` method in `FormGenerator` class we will write our process:

1. Before we start, we need to define the name space and add a number of helper-classes, so we will start with:
    ````php
    use DrupalCodeGenerator\Command\BaseGenerator;
    use DrupalCodeGenerator\Utils;
    use Symfony\Component\Console\Input\InputInterface;
    use Symfony\Component\Console\Output\OutputInterface;
    use Symfony\Component\Console\Question\ConfirmationQuestion;
    use Symfony\Component\Console\Question\Question;
    ````
2. we will ask the user inside which module he wants to add this form and to define his form class name.
    ````php
    // Determine the module to add our form to.
    $questions = Utils::moduleQuestions();
    // Suggest default class name: MyModuleForm
    $defaultClass = function ($vars) {
      return Utils::camelize($vars['machine_name']) . 'Form';
    };
    // Add a question to the question list.
    $questions['class'] = new Question('Class', $defaultClass);
    // Ask the questions
    $this->collectVars($input, $output, $questions);
    ````

3. Now we will ask the use if he would like to inject dependencies:
    ````php
     // Check if user wants to add services.
     $di_question = new ConfirmationQuestion(
       'Would you like to inject dependencies?', FALSE
     );
     if ($this->ask($input, $output, $di_question)) {
       // Add services.
       $this->collectServices($input, $output);
     }
    ````

4. Generate form routing,
   to keep my code clean I moved route generation function to trait
   class `my_module\src\Traits\FromRouteInteractionTrait.php`
    ````php

   /**
   * Use this trait to generate Routes.
   */
   trait FromRouteInteractionTrait {

     /**
      * Interacts with the user and builds route variables.
      */
     protected function routeInteraction(InputInterface $input, OutputInterface $output) {
       $vars = &$this->vars;

       $route_question = new ConfirmationQuestion(
         'Would you like to create a route for this form?'
       );
       $vars['route'] = $this->ask($input, $output, $route_question);

       $raw_form_id = preg_replace(
         '/_form/',
         '',
         Utils::camel2machine($vars['class'])
       );
       $vars['form_id'] = $vars['machine_name'] . '_' . $raw_form_id;

       if ($vars['route']) {
         $default_route_path = str_replace(
           '_',
           '-',
           $vars['machine_name'] . '/' . $raw_form_id
         );
         $route_questions['route_name'] = new Question(
           'Route name',
           '{machine_name}.' . $raw_form_id
         );
         $route_questions['route_path'] = new Question(
           'Route path',
           $default_route_path
         );
         $route_questions['route_title'] = new Question(
           'Route title',
           Utils::machine2human($raw_form_id)
         );
         $route_questions['route_permission'] = new Question(
           'Route permission',
           'administer site configuration'
         );

         $this->collectVars($input, $output, $route_questions, $vars);
         // Generate the file.
         $this->addFile()
           ->path('{machine_name}.routing.yml')
           ->template('form/routing.twig')
           ->action('append');
       }
     }
    }
    ````
5. Now we need to define `routing.twig` template inside `my_module\templates\form\routing.twig`
    ````php
    {{ route_name }}:
      path: '{{ route_path }}'
      defaults:
        _title: '{{ route_title }}'
        _form: 'Drupal\{{ machine_name }}\Form\{{ classForm }}'
      requirements:
        _permission: '{{ route_permission }}'
    ````
6. Inside our class we call the `routeInteraction` function:
    ````php
    // Generate Route
    $this->routeInteraction($input, $output);
    ````
7. Last step we generate our form template:
    ````php
    $this->addFile()
        ->path('src/Form/{class}.php')
        ->template('/form/form.twig');
    }
    ````
8. Now we need to create our ``form.twig`` template:

    ````php
    // This lib is used to inject services.
    {% import 'lib/di.twig' as di %}
    <?php

    namespace Drupal\{{ machine_name }}\Form;

    {% sort %}
      use Drupal\Core\Form\FormBase;
      use Drupal\Core\Form\FormStateInterface;

      {% if services %}
        use Symfony\Component\DependencyInjection\ContainerInterface;
        {{ di.use(services) }}
      {% endif %}
    {% endsort %}

    /**
    * Provides a {{ name }} form.
    */
    class {{ class }} extends FormBase {
      // We inject services.
      {% if services %}

        {{ di.properties(services) }}

        /**
        * The constructor.
        *
        {{ di.annotation(services) }}
        */
        public function __construct({{ di.signature(services) }}) {
          {{ di.assignment(services) }}
        }

        /**
        * {@inheritdoc}
        */
        public static function create(ContainerInterface $container) {
          return new static(
            {{ di.container(services) }}
          );
        }
      {% endif %}

      /**
      * {@inheritdoc}
      */
      public function getFormId() {
        return '{{ form_id }}';
      }

      /**
      * {@inheritdoc}
      */
      public function buildForm(array $form, FormStateInterface $form_state) {

        $form['message'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Message'),
          '#required' => TRUE,
        ];

        $form['actions'] = [
          '#type' => 'actions',
        ];
        $form['actions']['submit'] = [
          '#type' => 'submit',
          '#value' => $this->t('Send'),
        ];

        return $form;
      }

     /**
     * {@inheritdoc}
     */
     public function validateForm(array &$form, FormStateInterface $form_state) {
      if (mb_strlen($form_state->getValue('message')) < 10) {
        $form_state->setErrorByName('name', $this->t('Message should be at least 10 characters.'));
      }
     }

     /**
     * {@inheritdoc}
     */
     public function submitForm(array &$form, FormStateInterface $form_state) {
      $this->messenger()->addStatus($this->t('The message has been sent.'));
      $form_state->setRedirect('<front>');
     }
    }
   ````

To run this code:

````shell
  drush generate gnform
````

Conclusion
---------
Now you can define some blueprint code for all your projects and concentrate on the fun bits of Drupal-programing.



